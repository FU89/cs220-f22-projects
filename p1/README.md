# Project 1 (P1)

## Clarifications/Corrections

None yet.

**Find any issues?** Report to us:

- Ashwin Maran <amaran@wisc.edu>
- Isha Padmanaban <ipadmanaban@wisc.edu>
- Brandon Tran <bqtran2@wisc.edu>

## Learning Objectives

In addition to the learning objectives of Lab 1
* Download files from GitHub
* Use basic terminal commands
* Run a Python script
* Understand and use absolute and relative paths
* Create and run a Jupyter Notebook
* Download and run a Jupyter Notebook

you will learn to:
* Run otter tests
* Turn in your project using Gradescope

## Step 1: Setup

You should have a folder `cs220` that you created for lab-p1 under `Documents`. We're assuming you did the lab and know how to do all the Tasks we mentioned. If you're confused about any of the following steps, refer back to your lab.

#### Create a sub-folder called `p1` in your `cs220` folder
Refer to [Task 1.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p1#task-11-create-the-folders-for-this-lab) if you need help.  

This will store all your files related to p1. This way, you can keep files for different projects separate (you'll create a `p2` sub-folder for the next project and so on). Unfortunately, computers can crash and files can get accidentally deleted, so make sure you backup your work regularly (at a minimum, consider emailing yourself relevant files on occasion).

#### Download `p1.ipynb` to your `p1` folder.
Refer to [Task 1.7](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p1#task-17-correctly-download-a-python-file) if you need help.

#### Open a terminal in your `p1` folder.
Refer to [Task 1.3-1.5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p1#task-13-open-a-terminal-emulator-aka-a-terminal).

#### Run `p1.ipynb` code using jupyter notebook
Refer to [Task 4.1](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p1#task-41-start-up-a-juptyer-notebook).
Make sure to follow the directions provided in `p1.ipynb`.

**IMPORTANT NOTE**: Windows platform has a known issue: the last cell (containing `export`) WILL FAIL on Windows. Please ignore that.

## Submission instructions
- Create [Gradescope](https://www.gradescope.com/) login, if you haven't already created it. Invite should be available via your wisc email. **You must use your wisc email to create the Gradescope login**.
- Upload the zip file into P1 assignment. 
- *Make sure to check otter auto-grader results as soon as you can*. 
